﻿import-module 'C:\Program Files\Intel\Wired Networking\IntelNetCmdlets\IntelNetCmdlets.psd1'
import-module dcbqos
Import-Module DCB-test 

Write-Host -ForegroundColor cyan (Get-OS-Version)

$sut='192.168.1.40'
$lp ='192.168.1.10'

$ConnectionName=(Select-NIC)[0]
$IntelName=(Select-NIC)[1]
$ConnectionName1=(Select-NIC)[2]
$IntelName1=(Select-NIC)[3]

write-host -ForegroundColor Cyan "Selected NIC0: " $ConnectionName $IntelName
write-host -ForegroundColor Cyan "Selected NIC1: " $ConnectionName1 $IntelName1

write-host -ForegroundColor Cyan (Get-DCBVersion -IntelName $IntelName)
Get-DCB-Map -ConnectionName $ConnectionName

Set-DCBWilling
Set-DCBEnviroment -IntelNICName $IntelName
Set-VLAN3260 -ConnectionName $ConnectionName
Set-IP -IP $lp -ConnectionName $ConnectionName 
Clear-NetQoS


Start-Sleep 5


Set-OSDCBEnviroment -IntelNICName $IntelName
Get-DCB-Map -ConnectionName $ConnectionName

$bandtwith=44
New-NetQosTrafficClass -name "iSCSI"   -priority 4 -bandwidthPercentage $bandtwith -Algorithm ETS
New-NetQosPolicy -Name "UP4" -PriorityValue8021Action 4 -iSCSI
Enable-NetQosFlowControl -Priority 3,4

Disable-NetAdapter -Name $ConnectionName -Confirm:$false
Enable-NetAdapter -Name $ConnectionName -Confirm:$false

Start-sleep 60
$y = (Get-NetAdapterQoS –Name $ConnectionName).OperationalTrafficClasses

if ($y -like "*1 ETS    $bandtwith%       4*")  {Write-host -ForegroundColor green "Test 1: OK"} else {Write-host -ForegroundColor red "Test Failed"}

$z = (Get-IntelNetAdapterStatus -Status DCB -Name $IntelName) 
if (($z |Where-Object {$_.DisplayName -like 'iSCSI Bandwidth Percentage'}).DisplayValue -eq $bandtwith) {Write-host -ForegroundColor green "Test 2: OK"} else {Write-host -ForegroundColor red "Test Failed"}




Set-DCBWilling
Set-DCBEnviroment -IntelNICName $IntelName
Clear-NetQoS