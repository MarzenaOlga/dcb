
#DCB-Test Functions
####################################################################################################################################

Function Get-NIC  {
$nic=@{}
$nics = get-netadapter | Where-Object {$_.InterfaceDescription -notlike '*VLAN*'}
do 
{
if (!(Test-Path C:\DCB\iflist.txt)) {Add-NIC}
$content= @(get-content C:\DCB\iflist.txt)
Foreach ($co in $content)
    {
    foreach ($n in $nics)
        { 
        #if (($n.InterfaceDescription -like 'Intel(R) Ethernet Converged*') -or ($n.InterfaceDescription -like '*XXV710*') -or ($n.InterfaceDescription -like 'Intel(R) Ethernet Server Adapter X520*') ) 
        if ($n.InterfaceDescription -like $co)
            {
            if ($n.InterfaceDescription -like '*rNDC*'){}
            else {if ($n.Status -like 'Up'){$nic.add($n.Name,$n.ifDesc) }} 

            } 
        }
    }
if ($nic.Count -eq 0) {Add-NIC }
} while ($nic.Count -eq 0)
return $nic
}

####################################################################################################################################

Function Select-NIC {
    $Connections = Get-NIC
    $CN=@()
    $IN=@()
    $ret=@()
    Foreach ($x in $Connections.Keys){
        $CN += $x
        $IN += $Connections[$x]
    }

    for($i=0;$i -eq ($CN.Length); $i++){ 
      New-Variable -Name "ConnectionName$i" -Value $CN[$i]
      New-Variable -Name "IntelName$i" -Value $IN[$i] 
      Get-Variable -Name "ConnectionName$i" -ValueOnly      
      Get-Variable -Name "IntelName$i" -ValueOnly      
    }
    $ret = @($CN[0],$IN[0],$CN[1],$IN[1],$CN[2],$IN[2],$CN[3],$IN[3])   
return $ret
}

####################################################################################################################################

Function Start-Iperf-LP
{
 [CmdletBinding(DefaultParameterSetName='IntelNICName')]
param(  
        [Parameter(Mandatory=$true)]
        [String]$IPAddress,

        [Parameter(Mandatory=$true)]    
        [String]$IntelNICName = (Select-NIC)[1]   
     )


if ( $IntelNICName -like "Intel(R)*x5?0*") 
    {
    $priority=@(0,1,3,4)
    #$priority=@(0,1,2,3,4,5,6,7)
    foreach ($i in $priority){Start-Process cmd "/c `"title Priorytet $i && C:\posh\modules\ANVL\tools\iperf3\iperf3.exe -s $IPAddress -p 500$i -f g -  `""}
    }
else
    {
    $priority=@(0,1,2,3,4,5,6,7)
    foreach ($i in $priority){Start-Process cmd "/c `"title Priorytet $i && C:\posh\modules\ANVL\tools\iperf3\iperf3.exe -s $IPAddress -p 500$i -f g -  `""}
    }
}

####################################################################################################################################

Function Stop-Iperf
{
$test = Get-Process -Name 'Iperf*' -ea SilentlyContinue 
if ($test -ne $null) {Stop-Process -Name 'Iperf*' -Force -Confirm:$false}
}

####################################################################################################################################

Function Start-Iperf()
{
[CmdletBinding(DefaultParameterSetName='IntelNICName')]
param(  
        [Parameter(Mandatory=$true)]
        [String]$SutIPAddress,

        [Parameter(Mandatory=$true)]
        [String]$LpIPAddress,

        [Parameter(Mandatory=$true)]    
        [String]$IntelNICName = (Select-NIC)[1]
     )

$x=$null
While ($x -eq $null){
    $x = Invoke-Command -ComputerName $LpIPAddress -ScriptBlock {Get-Process -Name "Iperf*" -ea SilentlyContinue}
    Start-sleep 5}

if ($IntelNICName -like "Intel(R)*x5?0*") 
    {
    $priority=@(0,1,3,4)
    #$priority=@(0,1,2,3)
    foreach ($i in $priority){Start-Process cmd "/c `"title Priorytet $i && C:\posh\modules\ANVL\tools\iperf3\iperf3.exe -c $LpIPAddress -B $SutIPAddress -p 500$i -t 600 -i 1 -f g -P 4 - `""}
    }
else
    {
    $priority=@(0,1,2,3,4,5,6,7)
    foreach ($i in $priority){Start-Process cmd "/c `"title Priorytet $i && C:\posh\modules\ANVL\tools\iperf3\iperf3.exe -c $LpIPAddress -B $SutIPAddress -p 500$i -t 600 -i 1 -f g -P 4 - `""}
    }
}

####################################################################################################################################

Function Get-DCBVersion
{
param(  
        [Parameter(Mandatory=$true)]
        [String]$IntelName=(Select-NIC)[1]
     )
$status = Get-IntelNetAdapterStatus -Status DCB -Name $IntelName
Foreach ($st in $status){ if ($st.DisplayName.Equals('DCB Version'))  {$res1 = $st.DisplayValue}}
return $res1
}

####################################################################################################################################

Function Set-DCBEnviroment
{
param(  

        [Parameter(Mandatory=$true,
               ParameterSetName = 'IntelNICName',
                Position=0)]
        [Alias('NicName')]
        [String]
        $IntelNICName = (Select-NIC)[1]
     )

if ($IntelNICName -like "Intel(R)*x5?0*") 
    { 
        Set-IntelNetAdapterSetting -Name $IntelNICName -DisplayName "DCB" -DisplayValue "Switch Settings" 
        Start-Sleep 5
        if ((Get-IntelNetAdapterSetting -Name $IntelNICName -DisplayName 'DCB').RegistryValue -eq 1) {$a = 0} else {$a = 1}
    }
    else 
    {
         Set-IntelNetAdapterSetting -Name $IntelNICName -DisplayName "DCB" -DisplayValue "Enabled" 
         Start-Sleep 5
         if ((Get-IntelNetAdapterSetting -Name $IntelNICName -DisplayName 'DCB').RegistryValue -eq 5) {$a = 0} else {$a = 1}
    }


if ((Get-IntelNetAdapterSetting -Name $IntelNICName -DisplayName 'DCB Status').RegistryValue -eq 0 ) {$a = $a + 0} else {$a = $a + 1}
return $a
}

####################################################################################################################################

Function Set-DCBWilling
{
Set-NetQosDcbxSetting -Willing 1 -Confirm:$false
if ((Get-NetQosDcbxSetting).Willing) {return 0} else {return 1}
}

Function Set-DCBNoWilling
{
Set-NetQosDcbxSetting -Willing 0 -Confirm:$false
if (!(Get-NetQosDcbxSetting).Willing) {return 0} else {return 1}
}

####################################################################################################################################

Function Set-VLAN3260
{
[CmdletBinding(DefaultParameterSetName='ConnectionName')]
param(  
        [Parameter(Mandatory=$true,
               ParameterSetName = 'ConnectionName',
                Position=0)]
        [Alias('NicName')]
        [String]
        $ConnectionName = (Select-NIC)[0]
     )

$os = Get-OS-Version
if ($os -like '*2012 R2*')
    {
        $vlan = Get-IntelNetVLAN
        $IntelName=(get-netadapter -Name $ConnectionName).InterfaceDescription
        if (!($vlan.VLANID -eq '3260')){ 
            Add-IntelNetVLAN -ParentName $IntelName -VLANID 3260 
            Set-IntelNetVLAN -ParentName $IntelName -VLANID 3260 -NewVLANName VLAN3260            
            }
        if ((Get-IntelNetVLAN -ParentName $IntelName).VlanID -eq '3260') {return 0} else {return 1}

    }
else
    {
        $vlan = Get-NetAdapterAdvancedProperty -Name $ConnectionName -DisplayName 'VLAN ID'
        if (!($vlan -eq '3260')) {Set-NetAdapterAdvancedProperty -Name $ConnectionName -DisplayName 'VLAN ID' -DisplayValue '3260'} 
        if ((Get-NetAdapterAdvancedProperty -Name $ConnectionName -DisplayName 'VLAN ID').name -like $ConnectionName) {return 0} else {return 1}
    }

}

####################################################################################################################################

Function Set-VLAN860
{
[CmdletBinding(DefaultParameterSetName='ConnectionName')]
param(  
        [Parameter(Mandatory=$true,
               ParameterSetName = 'ConnectionName',
                Position=0)]
        [Alias('NicName')]
        [String]
        $ConnectionName = (Select-NIC)[0]
     )

$os = Get-OS-Version
if ($os -like '*2012 R2*')
    {
        $vlan = Get-IntelNetVLAN
        $IntelName=(get-netadapter -Name $ConnectionName).InterfaceDescription
        if (!($vlan.VLANID -eq '860')){ 
            Add-IntelNetVLAN -ParentName $IntelName -VLANID 860 
            Set-IntelNetVLAN -ParentName $IntelName -VLANID 860 -NewVLANName VLAN860            
            }
        if ((Get-IntelNetVLAN -ParentName $IntelName).VlanID -eq '860') {return 0} else {return 1}

    }
else
    {
        $vlan = Get-NetAdapterAdvancedProperty -Name $ConnectionName -DisplayName 'VLAN ID'
        if (!($vlan -eq '860')) {Set-NetAdapterAdvancedProperty -Name $ConnectionName -DisplayName 'VLAN ID' -DisplayValue '860'} 
        if ((Get-NetAdapterAdvancedProperty -Name $ConnectionName -DisplayName 'VLAN ID').name -like $ConnectionName) {return 0} else {return 1}
    }
}

####################################################################################################################################

Function Set-IP
{
param(  

        [Parameter(Mandatory=$true)]
        [String]$ConnectionName,
        [Parameter(Mandatory=$true)]
        [String] $IP 
        
     )
$os = Get-OS-Version
if ($os -like '*2012 R2*'){
    $x=(get-netadapter -Name $ConnectionName).InterfaceDescription 
    $Connection = (Get-NetAdapter |Where-Object {$_.InterfaceDescription -like "$x*VLAN3260*"}).Name
    if ($Connection -like '') { $Connection = (Get-NetAdapter |Where-Object {$_.InterfaceDescription -like "$x*VLAN860*"}).Name }
    #$Connection = (Get-NetAdapter |Where-Object {$_.InterfaceDescription -like "$ConnectionName*VLAN860*"}).Name
    }
else {$Connection = $ConnectionName }  

if (!(Test-Connection $IP  -quiet)){
    $id = (Get-NetAdapter -Name $Connection).ifIndex
    Get-NetIPAddress -InterfaceIndex $id | Remove-NetIPAddress -Confirm:$false 
    New-NetIPAddress -IPAddress $IP  -PrefixLength 24 -InterfaceIndex $id }
if (Test-Connection $IP -quiet) {return 0} else {return 1}
} 

####################################################################################################################################

Function Clear-NetQoS
{
Remove-NetQosPolicy -Name * -Confirm:$false
Remove-NetQosTrafficClass -Name * -Confirm:$false
Disable-NetQosFlowControl 0,1,2,3,4,5,6,7 -Confirm:$false
}

####################################################################################################################################

Function Set-QoSPolicy
{
param(  
        [Parameter(Mandatory=$true,
               ParameterSetName = 'IP',
                Position=0)]
        [Alias('NicIP')] [String] $IP 
)      

New-NetQosPolicy -Name "UP1 Bandwidth Test" -PriorityValue8021Action 1 -IPSrcPrefixMatchCondition $IP -IPDstPortStartMatchCondition 5001 -IPDstPortEndMatchCondition 5001
New-NetQosPolicy -Name "UP2 Bandwidth Test" -PriorityValue8021Action 2 -IPSrcPrefixMatchCondition $IP -IPDstPortStartMatchCondition 5002 -IPDstPortEndMatchCondition 5002
New-NetQosPolicy -Name "UP3 Bandwidth Test" -PriorityValue8021Action 3 -IPSrcPrefixMatchCondition $IP -IPDstPortStartMatchCondition 5003 -IPDstPortEndMatchCondition 5003
New-NetQosPolicy -Name "UP4 Bandwidth Test" -PriorityValue8021Action 4 -IPSrcPrefixMatchCondition $IP -IPDstPortStartMatchCondition 5004 -IPDstPortEndMatchCondition 5004
New-NetQosPolicy -Name "UP5 Bandwidth Test" -PriorityValue8021Action 5 -IPSrcPrefixMatchCondition $IP -IPDstPortStartMatchCondition 5005 -IPDstPortEndMatchCondition 5005
New-NetQosPolicy -Name "UP6 Bandwidth Test" -PriorityValue8021Action 6 -IPSrcPrefixMatchCondition $IP -IPDstPortStartMatchCondition 5006 -IPDstPortEndMatchCondition 5006
New-NetQosPolicy -Name "UP7 Bandwidth Test" -PriorityValue8021Action 7 -IPSrcPrefixMatchCondition $IP -IPDstPortStartMatchCondition 5007 -IPDstPortEndMatchCondition 5007
}

####################################################################################################################################

Function Get-DCB-Map
{
param(  
        [Parameter(Mandatory=$true,
               ParameterSetName = 'ConnectionName',
                Position=0)]
        [Alias('NicName')]
        [String]$ConnectionName = (Select-NIC)[0]
     )
$test = Get-NetAdapterQoS –Name $ConnectionName
$res = @{Remote ='Other';Operational = 'Other'}
if (($test.RemoteTrafficClasses -like ('*1 ETS    30%       3*')) -and ($test.RemoteTrafficClasses -like ('*2 ETS    30%       4*')) -and ($test.RemoteTrafficClasses -like ('*0 ETS    40%       0-2,5-7*')))  {$res.Remote = 'SAN_DCB_MAP'}
if (($test.RemoteTrafficClasses -like ('*1 ETS    40%       4*')) -and ($test.RemoteTrafficClasses -like ('*0 ETS    60%       0-3,5-7*')))  {$res.Remote =  'SAN_DCB_ISCSI_MAP'}
if (($test.OperationalTrafficClasses -like ('*1 ETS    30%       3*')) -and ($test.OperationalTrafficClasses -like ('*2 ETS    30%       4*')) -and ($test.OperationalTrafficClasses -like ('*0 ETS    40%       0-2,5-7*')))  {$res.Operational =  'SAN_DCB_MAP'}
if (($test.OperationalTrafficClasses -like ('*1 ETS    40%       4*')) -and ($test.OperationalTrafficClasses -like ('*0 ETS    60%       0-3,5-7*')))  {$res.Operational = 'SAN_DCB_ISCSI_MAP'}



return $res
}

####################################################################################################################################

Function Get-OS-Version
{
return (Get-WmiObject -class Win32_OperatingSystem).Caption + " build " + ([environment]::OSVersion.Version).build 
}

####################################################################################################################################

Function Get-TCP-Dump
{
param(  
        [Parameter(Mandatory=$true)]
        [int]$Interface=0
     )
$ret = @()
if (!(Test-Path c:\temp)){New-item -ItemType Directory -Path 'c:\' -Name 'temp'}
C:\DCB\Tools\TCPDump\tcpdump.exe -ne -i $Interface -c 1000 > C:\temp\tcp_dump.txt 
$dump = Get-content C:\temp\tcp_dump.txt 
foreach ($d in $dump){  if ($d -like '*P.*') {$ret += $d } }
return $ret
}

####################################################################################################################################

Function Disable-DCBEnviroment
{
param(  

        [Parameter(Mandatory=$true,
               ParameterSetName = 'IntelNICName',
                Position=0)]
        [Alias('NicName')]
        [String]
        $IntelNICName = (Select-NIC)[1]
     )
$a=0
if ($IntelNICName -like "Intel(R)*x5?0*") 
    { 
        Set-IntelNetAdapterSetting -Name $IntelNICName -DisplayName "DCB" -DisplayValue "Disabled" 
        Start-Sleep 5
        if ((Get-IntelNetAdapterSetting -Name $IntelNICName -DisplayName 'DCB').RegistryValue -eq 0) {$a = 0} else {$a = 1}
    }
    else 
    {
         Set-IntelNetAdapterSetting -Name $IntelNICName -DisplayName "DCB" -DisplayValue "Disabled" 
         Start-Sleep 5
         if ((Get-IntelNetAdapterSetting -Name $IntelNICName -DisplayName 'DCB').RegistryValue -eq 0) {$a = 0} else {$a = 1}
    }
if ((Get-IntelNetAdapterSetting -Name $IntelNICName -DisplayName 'DCB Status').RegistryValue -eq 3 ) {$a = $a + 0} else {$a = $a + 1}
return $a
}

####################################################################################################################################

function Set-iSCSI-Disk
{
param(  
        [Parameter(Mandatory=$true)]
        [String]$DriveLetter,
        [Parameter(Mandatory=$true)]
        [String]$LinkPartner,
        [Parameter(Mandatory=$true)]
        [String]$SUT
     )
$ret=0
if ($DriveLetter.Length -eq 1){$ret =0} else {$ret =1} 
$DriveLetter2 = $DriveLetter+':'
if (!(Test-Path $DriveLetter2)){
    if ((Get-Service -Name "MSiSCSI").Status -like 'Stopped'){ Set-Service -Name "MSiSCSI" -Status Running -StartupType Automatic }
    if ((Get-Service -Name "MSiSCSI").Status -like 'Stopped'){ $ret = $ret+10 }
    else {
        if (!(Get-iscsitarget).IsConnected) {
            New-IscsiTargetPortal –TargetPortalAddress $LinkPartner -InitiatorPortalAddress $SUT -InitiatorInstanceName "ROOT\ISCSIPRT\0000_0"
            Get-iscsitarget | Connect-iscsitarget -targetportaladdress $LinkPartner -ispersistent $true -initiatorportaladdress $SUT |Out-Null
        }
        if (!(Get-iscsitarget).IsConnected) {$ret = $ret+100 }
        if ((Get-iscsitarget).IsConnected){
            $disks = Get-Disk
            #Foreach ($w in $disks){ if (!($w.OperationalStatus -like 'Online'))   {$z = $w.Number; $x=$w.FriendlyName }} 
            Foreach ($w in $disks){ if (($w.FriendlyName -like 'MSFT Virtual HD'))   {$z = $w.Number; $x=$w.FriendlyName }} 
            #if ((get-disk -FriendlyName $x).OperationalStatus -ne 'Online')  {Initialize-Disk -FriendlyName $x}
            Initialize-Disk -FriendlyName $x
            Set-Disk -Number $z -IsOffline $false
            Set-Disk -Number $z -IsReadOnly $false
            
            New-Partition -DiskNumber $z -UseMaximumSize -DriveLetter $DriveLetter
            Initialize-Volume -DriveLetter $DriveLetter
        }
        start-sleep 5
        if (!(Test-Path $DriveLetter2)) {$ret = $ret +1000}
        }
    return $ret
}
else {return $ret }
}

####################################################################################################################################

Function Test-NetQosPolicy
{
param(  
        [Parameter(Mandatory=$true)]
        [String]$LinkPartner,
        [Parameter(Mandatory=$true)]
        [String]$SUT
     )
if (!(Get-NetQosPolicy).IPDstPortStart -eq '3260'){
    $Target = Get-IscsiTarget
    Disconnect-IscsiTarget -NodeAddress $Target.NodeAddress  -Confirm:$false
    Update-IscsiTargetPortal -TargetPortalAddress $LinkPartner -InitiatorPortalAddress $SUT -InitiatorInstanceName "ROOT\ISCSIPRT\0000_0"
    Connect-iscsitarget -targetportaladdress $LinkPartner -ispersistent $true -initiatorportaladdress $SUT -NodeAddress $Target.NodeAddress -ea SilentlyContinue 
    if ((Get-NetQosPolicy).IPDstPortStart -eq '3260'){return 0} else {return 1}
    }
else {Return 0}
}


Function Set-QoSTraffic 
{
param(  

        [Parameter(Mandatory=$true,
               ParameterSetName = 'IntelNICName',
                Position=0)]
        [Alias('NicName')]
        [String]
        $IntelNICName = (Select-NIC)[1]
     )
if ($IntelNICName -like "Intel(R)*x5?0*")
    {
    New-NetQosTrafficClass -Name "UP1 Bandwidth Test" -priority 1 -BandwidthPercentage 10 -Algorithm ETS
    New-NetQosTrafficClass -Name "UP3 Bandwidth Test" -priority 3 -BandwidthPercentage 25 -Algorithm ETS
    New-NetQosTrafficClass -Name "UP4 Bandwidth Test" -priority 4 -BandwidthPercentage 35 -Algorithm ETS
    }
else
    {
    New-NetQosTrafficClass -Name "UP1 Bandwidth Test" -priority 1 -BandwidthPercentage 5 -Algorithm ETS
    New-NetQosTrafficClass -Name "UP2 Bandwidth Test" -priority 2 -BandwidthPercentage 7 -Algorithm ETS
    New-NetQosTrafficClass -Name "UP3 Bandwidth Test" -priority 3 -BandwidthPercentage 10 -Algorithm ETS
    New-NetQosTrafficClass -Name "UP4 Bandwidth Test" -priority 4 -BandwidthPercentage 15 -Algorithm ETS
    New-NetQosTrafficClass -Name "UP5 Bandwidth Test" -priority 5 -BandwidthPercentage 17 -Algorithm ETS
    New-NetQosTrafficClass -Name "UP6 Bandwidth Test" -priority 6 -BandwidthPercentage 20 -Algorithm ETS
    New-NetQosTrafficClass -Name "UP7 Bandwidth Test" -priority 7 -BandwidthPercentage 23 -Algorithm ETS
    }
}

#######################################################

Function Set-OSDCBEnviroment
{
param(  

        [Parameter(Mandatory=$true,
               ParameterSetName = 'IntelNICName',
                Position=0)]
        [Alias('NicName')]
        [String]
        $IntelNICName = (Select-NIC)[1]
     )
$a=0
if ($IntelNICName -like "Intel(R)*x5?0*") 
    { 
        Set-IntelNetAdapterSetting -Name $IntelNICName -DisplayName "DCB" -DisplayValue "OS Controlled" 
        Start-Sleep 5
        if ((Get-IntelNetAdapterSetting -Name $IntelNICName -DisplayName 'DCB').RegistryValue -eq 3) {$a = 0} else {$a = 1}
    }
    else 
    {
         Set-IntelNetAdapterSetting -Name $IntelNICName -DisplayName "DCB" -DisplayValue "Enabled" 
         Start-Sleep 5
         if ((Get-IntelNetAdapterSetting -Name $IntelNICName -DisplayName 'DCB').RegistryValue -eq 5) {$a = 0} else {$a = 1}
    }

if ((Get-IntelNetAdapterSetting -Name $IntelNICName -DisplayName 'DCB Status').RegistryValue -eq 0 ) {$a = $a + 0} else {$a = $a + 1}
$a = $a + (Set-DCBNoWilling) 
return $a
}

#########################################################
Function Get-RQMData
{
   [Parameter(Mandatory=$true)]
   [String]$IntelName


if (Test-Path C:\data\v*) {
    $driver = Get-IntelNetAdapter -Name $IntelName | select name,etrackid,driver*
    $os = Get-OS-Version
    $buildno = gci c:\data\v* | sort LastWriteTime | select -last 1
    $str = "c:\data\"+($buildno).Name+"\*" 
    $container = gci $str | sort LastWriteTime | select -last 1
    $str2="c:\data\"+($buildno).Name+"\"+$container.Name+"\verfile.tic"
    $tic= Get-Content $str2 
    $ret=[ordered]@{NIC=$driver.name;Build=$buildno.Name;Version=$driver.DriverVersion;PBA="-";OS=$os;TIC=$tic;Container=$container.Name;NVM=$driver.etrackid}
    return $ret
    }

if (Test-Path D:\data\v*) {
    $driver = Get-IntelNetAdapter -Name $IntelName | select name,etrackid,driver*
    $os = Get-OS-Version
    $buildno = gci D:\data\v* | sort LastWriteTime | select -last 1
    $str = "c:\data\"+($buildno).Name+"\*" 
    $container = gci $str | sort LastWriteTime | select -last 1
    $str2="D:\data\"+($buildno).Name+"\"+$container.Name+"\verfile.tic"
    $tic= Get-Content $str2
    $ret=[ordered]@{NIC=$driver.name;Build=$buildno.Name;Version=$driver.DriverVersion;PBA="-";OS=$os;TIC=$tic;Container=$container.Name;NVM=$driver.etrackid}
    return $ret
    }
}

##################################################################################
Function Add-NIC
{
$list = (Get-NetAdapter) | Where-Object {($_.InterfaceDescription -like 'Intel(R)*') -and ($_.Status -like 'Up') }
$i=0
Foreach ($li in $list) {write-host $i'.' $li.Name '-' $li.InterfaceDescription;$i++}
do {
[int]$select = read-host('Select NIC:') }
while (!(($select -ge 0) -and ($select -lt $i)))   
if (!(Test-Path C:\DCB\iflist.txt)) {New-Item -ItemType file -Path C:\DCB -Name iflist.txt}
$content= @(get-content C:\DCB\iflist.txt)
$content += $list[$select].InterfaceDescription
$Content | Out-File C:\DCB\iflist.txt
}