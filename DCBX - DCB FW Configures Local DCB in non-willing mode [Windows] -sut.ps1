﻿import-module 'C:\Program Files\Intel\Wired Networking\IntelNetCmdlets\IntelNetCmdlets.psd1'
import-module dcbqos
Import-Module DCB-test 

Write-Host -ForegroundColor cyan (Get-OS-Version)

$sut='192.168.1.40'
$lp ='192.168.1.10'

$ConnectionName=(Select-NIC)[0]
$IntelName=(Select-NIC)[1]
$ConnectionName1=(Select-NIC)[2]
$IntelName1=(Select-NIC)[3]

write-host -ForegroundColor Cyan "Selected NIC0: " $ConnectionName $IntelName
write-host -ForegroundColor Cyan "Selected NIC1: " $ConnectionName1 $IntelName1

write-host -ForegroundColor Cyan (Get-DCBVersion -IntelName $IntelName)
Get-DCB-Map -ConnectionName $ConnectionName

Set-DCBWilling
Set-DCBEnviroment -IntelNICName $IntelName
Set-VLAN3260 -ConnectionName $ConnectionName
Set-IP -IP $sut -ConnectionName $ConnectionName 
Clear-NetQoS


Start-Sleep 5


Set-OSDCBEnviroment -IntelNICName $IntelName
Get-DCB-Map -ConnectionName $ConnectionName
Start-sleep 60
if ((Get-NetAdapterQos -Name $ConnectionName).OperationalTrafficClasses -like '*0 ETS    100%      0-7*'){Write-host -ForegroundColor green "Test : OK"} else {Write-host -ForegroundColor red "Test Failed"}

Set-DCBWilling
Set-DCBEnviroment -IntelNICName $IntelName