﻿import-module 'C:\Program Files\Intel\Wired Networking\IntelNetCmdlets\IntelNetCmdlets.psd1'
import-module dcbqos
Import-Module DCB-test 

Write-Host -ForegroundColor cyan (Get-OS-Version)

$sut='192.168.1.50'
$lp ='192.168.1.10'

$ConnectionName=(Select-NIC)[0]
$IntelName=(Select-NIC)[1]
$ConnectionName1=(Select-NIC)[2]
$IntelName1=(Select-NIC)[3]

write-host -ForegroundColor Cyan "Selected NIC0: " $ConnectionName $IntelName
write-host -ForegroundColor Cyan "Selected NIC1: " $ConnectionName1 $IntelName1

write-host -ForegroundColor Cyan (Get-DCBVersion -IntelName $IntelName)
Write-host -ForegroundColor Cyan (Get-DCB-Map -ConnectionName $ConnectionName )

Set-DCBEnviroment -IntelNICName $IntelName
Set-DCBWilling
Set-VLAN3260 -ConnectionName $ConnectionName
Set-IP -IP $sut 
Clear-NetQoS

Start-Sleep 5

Write-host -ForegroundColor Cyan "Start test"

Set-DCBNoWilling
if ($IntelName -like "Intel(R)*x5?0*"){ Set-IntelNetAdapterSetting -Name $IntelName -DisplayName "DCB" -DisplayValue "OS Controlled" }
Set-QoSPolicy -IP $lp
Set-QoSTraffic -IntelNICName $IntelName
Enable-NetQosFlowControl -Priority 0

$packets=@('1514','4088','9014')
$i=1
$ti=60
Foreach ($pack in $packets){
    Write-host -ForegroundColor Cyan "Step $i - Jumbo Packet: $pack"
    Set-NetAdapterAdvancedProperty -Name $ConnectionName -RegistryKeyword "*jumbopacket" -RegistryValue $pack
    if (((Get-NetAdapterAdvancedProperty -Name $ConnectionName  -DisplayName “Jumbo Packet”).RegistryValue) -eq ($pack)) {Write-host -ForegroundColor Green "Jumbo Packet $pack : Pass"} else {Write-host -ForegroundColor Red "Jumbo Packet $pack : Fail"}
    Start-Sleep 10
    Start-Iperf -SutIPAddress $sut -LpIPAddress $lp -IntelNICName $IntelName
    Write-host -ForegroundColor Cyan "Step time $ti sec"
    Start-sleep $ti
    Stop-Iperf 
    $i++
}

$z = Invoke-Command -ComputerName '10.102.229.148' -ScriptBlock {Select-NIC }
Invoke-Command -ComputerName '10.102.229.148' -ScriptBlock {Import-Module DCB-test }
Invoke-Command -ComputerName '10.102.229.148' -ScriptBlock {Stop-Iperf}
$nic = $z[1]
#$NewScriptBlock = [scriptblock]::Create("Start-Iperf-LP -IPAddress $lp -IntelNICName '$nic'")
#Invoke-Command -ComputerName '10.102.229.148' -ScriptBlock {$NewScriptBlock }
$nic = $z[0]
$NewScriptBlock = [scriptblock]::Create("Set-NetAdapterAdvancedProperty -Name '$nic' -RegistryKeyword '*jumbopacket' -RegistryValue '1514'")
Invoke-Command -ComputerName '10.102.229.148' -ScriptBlock {$NewScriptBlock }