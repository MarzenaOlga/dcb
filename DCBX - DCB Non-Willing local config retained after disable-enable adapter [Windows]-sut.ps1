﻿
import-module 'C:\Program Files\Intel\Wired Networking\IntelNetCmdlets\IntelNetCmdlets.psd1'
import-module dcbqos
Import-Module DCB-test 

Write-Host -ForegroundColor cyan (Get-OS-Version)

$sut='192.168.1.40'
$lp ='192.168.1.10'

$ConnectionName=(Select-NIC)[0]
$IntelName=(Select-NIC)[1]
$ConnectionName1=(Select-NIC)[2]
$IntelName1=(Select-NIC)[3]

write-host -ForegroundColor Cyan "Selected NIC0: " $ConnectionName $IntelName
write-host -ForegroundColor Cyan "Selected NIC1: " $ConnectionName1 $IntelName1

write-host -ForegroundColor Cyan (Get-DCBVersion -IntelName $IntelName)
Get-DCB-Map -ConnectionName $ConnectionName

Set-DCBWilling
Set-DCBEnviroment -IntelNICName $IntelName
Set-VLAN3260 -ConnectionName $ConnectionName
Set-IP -IP $sut -ConnectionName $ConnectionName 
Clear-NetQoS



Start-Sleep 5

Write-host -ForegroundColor Cyan "Start test"
$bandtwith=44
New-NetQosTrafficClass -name "iSCSI"   -priority 4 -bandwidthPercentage $bandtwith -Algorithm ETS -ea SilentlyContinue
New-NetQosPolicy -Name "UP4" -PriorityValue8021Action 4 -iSCSI -ea SilentlyContinue
Enable-NetQosFlowControl -Priority 3,4 -ea SilentlyContinue


Get-NetQosTrafficClass
Get-NetQosPolicy
Get-NetQosFlowControl


Set-OSDCBEnviroment -IntelNICName $IntelName
Start-sleep 10
Get-DCB-Map -ConnectionName $ConnectionName
$y = (Get-NetAdapterQoS –Name $ConnectionName).OperationalTrafficClasses
if ($y -like "*1 ETS    $bandtwith%       4*")  {Write-host -ForegroundColor green "Test 1: OK"} else {Write-host -ForegroundColor red "Test Failed"}

$z = (Get-IntelNetAdapterStatus -Status DCB -Name $IntelName) 
if (($z |Where-Object {$_.DisplayName -like 'iSCSI Bandwidth Percentage'}).DisplayValue -eq $bandtwith) {Write-host -ForegroundColor green "Test 2: OK"} else {Write-host -ForegroundColor red "Test Failed"}

Disable-NetAdapter -Name $ConnectionName -Confirm:$false
if ($ConnectionName1 -ne $null) {Disable-NetAdapter -Name $ConnectionName1 -Confirm:$false}
start-sleep 10
Enable-NetAdapter -Name $ConnectionName
if ($ConnectionName1 -ne $null) {Enable-NetAdapter -Name $ConnectionName1}
Start-sleep 10

Get-DCB-Map -ConnectionName $ConnectionName
$y = (Get-NetAdapterQoS –Name $ConnectionName).OperationalTrafficClasses
if ($y -like "*1 ETS    $bandtwith%       4*")  {Write-host -ForegroundColor green "Test 3: OK"} else {Write-host -ForegroundColor red "Test Failed"}

#$x1=Get-Date

$z = (Get-IntelNetAdapterStatus -Status DCB -Name $IntelName) 
if (($z |Where-Object {$_.DisplayName -like 'iSCSI Bandwidth Percentage'}).DisplayValue -eq $bandtwith) {Write-host -ForegroundColor green "Test 4: OK"} else {Write-host -ForegroundColor red "Test Failed"}

#$x2=Get-Date
#write-host ($x2 - $x1).Seconds

#(Get-IntelNetAdapterStatus -Status DCB -Name $IntelName) |Where-Object {$_.DisplayName -like 'iSCSI Bandwidth Percentage'}

#start-sleep 60



Set-DCBEnviroment -IntelNICName $IntelName
Set-DCBWilling
Clear-NetQoS
Get-IntelNetAdapterStatus -Status DCB -Name $IntelName



