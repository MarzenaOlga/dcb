﻿import-module 'C:\Program Files\Intel\Wired Networking\IntelNetCmdlets\IntelNetCmdlets.psd1'
import-module dcbqos
Import-Module DCB-test 

Write-Host -ForegroundColor cyan (Get-OS-Version)

$sut='192.168.1.40'
$lp ='192.168.1.10'

$ConnectionName=(Select-NIC)[0]
$IntelName=(Select-NIC)[1]
$ConnectionName1=(Select-NIC)[2]
$IntelName1=(Select-NIC)[3]

write-host -ForegroundColor Cyan "Selected NIC0: " $ConnectionName $IntelName
write-host -ForegroundColor Cyan "Selected NIC1: " $ConnectionName1 $IntelName1

write-host -ForegroundColor Cyan (Get-DCBVersion -IntelName $IntelName)
Get-DCB-Map -ConnectionName $ConnectionName

Set-DCBWilling
Set-DCBEnviroment -IntelNICName $IntelName
Set-VLAN3260 -ConnectionName $ConnectionName
Set-IP -IP $sut -ConnectionName $ConnectionName 
Clear-NetQoS
Start-Sleep 5
Write-Host -ForegroundColor Cyan "Start Test"
$dcbx1 = (Get-DCBVersion -IntelName $IntelName)
Write-host -ForegroundColor Green $dcbx1
Write-Host -ForegroundColor Yellow 'Please change "Dcbx version" on switch port'
do{
$dcbx2 = (Get-DCBVersion -IntelName $IntelName)
Write-host -ForegroundColor Cyan "$dcbx2"
} while ($dcbx1 -eq $dcbx2)

If ($dcbx1 -ne $dcbx2) {Write-host -ForegroundColor green "Test : OK"} else {Write-host -ForegroundColor red "Test Failed"}
