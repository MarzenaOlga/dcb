﻿
#$ComputerName = '10.102.229.166'
#& "C:\Users\Administrator\Documents\remote_session.ps1"

import-module 'C:\Program Files\Intel\Wired Networking\IntelNetCmdlets\IntelNetCmdlets.psd1'
import-module dcbqos
Import-Module DCB-test 

Write-Host -ForegroundColor cyan (Get-OS-Version)

$sut='192.168.1.50'
$lp ='192.168.1.10'

$ConnectionName=(Select-NIC)[0]
$IntelName=(Select-NIC)[1]
$ConnectionName1=(Select-NIC)[2]
$IntelName1=(Select-NIC)[3]

write-host -ForegroundColor Cyan "Selected NIC0: " $ConnectionName $IntelName
write-host -ForegroundColor Cyan "Selected NIC1: " $ConnectionName1 $IntelName1

write-host -ForegroundColor Cyan (Get-DCBVersion -IntelName $IntelName)
write-host -ForegroundColor Cyan (Get-DCBVersion -IntelName $IntelName1)
Write-host -ForegroundColor Cyan (Get-DCB-Map -ConnectionName $ConnectionName)
Write-host -ForegroundColor Cyan (Get-DCB-Map -ConnectionName $ConnectionName1)

Set-DCBEnviroment -IntelNICName $IntelName
Set-DCBEnviroment -IntelNICName $IntelName1
Set-DCBWilling
Clear-NetQoS

Start-Sleep 5
Write-host -ForegroundColor Cyan "Start test"


Disable-DCBEnviroment -IntelNICName $IntelName
Disable-DCBEnviroment -IntelNICName $IntelName1


Disable-NetAdapter $ConnectionName -Confirm:$false
Disable-NetAdapter $ConnectionName1 -Confirm:$false


Set-NetAdapterAdvancedProperty -name $ConnectionName -RegistryKeyword *QOS -RegistryValue 0
Set-NetAdapterAdvancedProperty -name $ConnectionName1 -RegistryKeyword *QOS -RegistryValue 0
Get-NetAdapterAdvancedProperty -name $ConnectionName -RegistryKeyword *QOS
Get-NetAdapterAdvancedProperty -name $ConnectionName1 -RegistryKeyword *QOS

Set-NetAdapterAdvancedProperty -name $ConnectionName1  -RegistryKeyword *QOS -RegistryValue 1
Get-NetAdapterAdvancedProperty -name $ConnectionName1 -RegistryKeyword *QOS
Enable-NetAdapter $ConnectionName1 -Confirm:$false
Write-host -ForegroundColor Cyan (Get-DCB-Map -ConnectionName $ConnectionName1)
Get-IntelNetAdapterSetting -Name $IntelName1 -DisplayName 'DCB'

Enable-NetAdapter $ConnectionName -Confirm:$false
Get-IntelNetAdapterSetting -Name $IntelName1 -DisplayName 'DCB'


Get-IntelNetAdapterStatus -Status DCB -Name $IntelName1
write-host -ForegroundColor Cyan (Get-DCBVersion -IntelName $IntelName1)
Get-NetAdapterQoS –Name $ConnectionName1

