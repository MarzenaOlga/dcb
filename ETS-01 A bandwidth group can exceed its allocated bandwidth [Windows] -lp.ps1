﻿import-module 'C:\Program Files\Intel\Wired Networking\IntelNetCmdlets\IntelNetCmdlets.psd1'
import-module dcbqos
Import-Module DCB-test 

Write-Host -ForegroundColor cyan (Get-OS-Version)

$sut='192.168.1.50'
$lp ='192.168.1.10'

$ConnectionName=(Select-NIC)[2]
$IntelName=(Select-NIC)[3]
$ConnectionName1=(Select-NIC)[0]
$IntelName1=(Select-NIC)[1]

write-host -ForegroundColor Cyan "Selected NIC0: " $ConnectionName $IntelName
write-host -ForegroundColor Cyan "Selected NIC1: " $ConnectionName1 $IntelName1

write-host -ForegroundColor Cyan (Get-DCBVersion -IntelName $IntelName)
Write-host -ForegroundColor Cyan (Get-DCB-Map -ConnectionName $ConnectionName )

Set-DCBEnviroment -IntelNICName $IntelName
Set-DCBWilling
Set-VLAN3260 -ConnectionName $ConnectionName
Set-IP -IP $lp 
Clear-NetQoS

Start-Sleep 5

Write-host -ForegroundColor Cyan "Start test"

Set-QoSPolicy -IP $lp

Start-Iperf-LP -IPAddress $lp 
