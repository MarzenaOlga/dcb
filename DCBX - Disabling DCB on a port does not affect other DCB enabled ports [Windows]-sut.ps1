﻿
import-module 'C:\Program Files\Intel\Wired Networking\IntelNetCmdlets\IntelNetCmdlets.psd1'
import-module dcbqos
Import-Module DCB-test 

Write-Host -ForegroundColor cyan (Get-OS-Version)

$sut='192.168.1.40'
$lp ='192.168.1.10'


Enable-NetAdapter (Get-NetAdapter |Where-Object {$_.InterfaceDescription -like 'Intel(R) Ethernet*'}).Name
start-sleep 5

$ConnectionName=(Select-NIC)[0]
$IntelName=(Select-NIC)[1]
$ConnectionName1=(Select-NIC)[2]
$IntelName1=(Select-NIC)[3]

write-host -ForegroundColor Cyan "Selected NIC0: " $ConnectionName $IntelName
write-host -ForegroundColor Cyan "Selected NIC1: " $ConnectionName1 $IntelName1

write-host -ForegroundColor Cyan (Get-DCBVersion -IntelName $IntelName )
write-host -ForegroundColor Cyan (Get-DCBVersion -IntelName $IntelName1 )
Get-DCB-Map -ConnectionName $ConnectionName
Get-DCB-Map -ConnectionName $ConnectionName1

Set-DCBEnviroment -IntelNICName $IntelName
Set-DCBEnviroment -IntelNICName $IntelName1

write-host -ForegroundColor Cyan (Get-DCBVersion -IntelName $IntelName )
write-host -ForegroundColor Cyan (Get-DCBVersion -IntelName $IntelName1 )
Get-DCB-Map -ConnectionName $ConnectionName
Get-DCB-Map -ConnectionName $ConnectionName1

Set-DCBWilling
#Set-VLAN3260 -ConnectionName $ConnectionName
#Set-IP -IP $sut -ConnectionName $ConnectionName
Clear-NetQoS
Start-Sleep 5
Write-host -ForegroundColor Cyan "Start test"

Disable-NetAdapter $ConnectionName -Confirm:$false
Disable-NetAdapter $ConnectionName1 -Confirm:$false

Set-NetAdapterAdvancedProperty -name $ConnectionName -RegistryKeyword *QOS -RegistryValue 0
Set-NetAdapterAdvancedProperty -name $ConnectionName1 -RegistryKeyword *QOS -RegistryValue 0

Get-NetAdapterAdvancedProperty -name $ConnectionName -RegistryKeyword *QOS
Get-NetAdapterAdvancedProperty -name $ConnectionName1 -RegistryKeyword *QOS


Set-NetAdapterAdvancedProperty -name $ConnectionName -RegistryKeyword *QOS -RegistryValue 1
#Set-NetAdapterAdvancedProperty -name $ConnectionName1 -RegistryKeyword *QOS -RegistryValue 1
Get-NetAdapterAdvancedProperty -name $ConnectionName -RegistryKeyword *QOS
Get-NetAdapterAdvancedProperty -name $ConnectionName1 -RegistryKeyword *QOS

Enable-NetAdapter -name $ConnectionName -Confirm:$false
Start-Sleep 15

Get-NetAdapterQoS –Name $ConnectionName
$a = Get-DCB-Map -ConnectionName $ConnectionName

if ($a.Operational -like 'SAN_DCB_ISCSI_MAP') {$bandtwith = 40} 
if ($a.Operational -like 'SAN_DCB_MAP') {$bandtwith = 30} 


$y = (Get-NetAdapterQoS –Name $ConnectionName).OperationalTrafficClasses
if ($y -like "* ETS    $bandtwith%       4*")  {Write-host -ForegroundColor green "Test 1: OK"} else {Write-host -ForegroundColor red "Test Failed"}


$x1=Get-Date
$z = (Get-IntelNetAdapterStatus -Status DCB -Name $IntelName) 
if (($z |Where-Object {$_.DisplayName -like 'iSCSI Bandwidth Percentage'}).DisplayValue -eq $bandtwith) {Write-host -ForegroundColor green "Test 2: OK"} else {Write-host -ForegroundColor red "Test Failed"}

$x2=Get-Date
write-host -ForegroundColor Red ($x2 - $x1)


$x1=Get-Date


Get-IntelNetAdapterStatus -Status DCB -Name $IntelName

$x2=Get-Date
write-host -ForegroundColor Yellow ($x2 - $x1)

Enable-NetAdapter -name $ConnectionName1 -Confirm:$false
Set-NetAdapterAdvancedProperty -name $ConnectionName1 -RegistryKeyword *QOS -RegistryValue 1
Start-sleep 15
Get-NetAdapterQoS –Name $ConnectionName
Get-DCB-Map -ConnectionName $ConnectionName
Get-DCB-Map -ConnectionName $ConnectionName1