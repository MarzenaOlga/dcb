﻿
#$ComputerName = '10.102.229.166'
#& "C:\Users\Administrator\Documents\remote_session.ps1"

import-module 'C:\Program Files\Intel\Wired Networking\IntelNetCmdlets\IntelNetCmdlets.psd1'
import-module dcbqos
Import-Module DCB-test 

Write-Host -ForegroundColor cyan (Get-OS-Version)

$sut='192.168.1.40'
$lp ='192.168.1.10'

$ConnectionName=(Select-NIC)[0]
$IntelName=(Select-NIC)[1]
$ConnectionName1=(Select-NIC)[2]
$IntelName1=(Select-NIC)[3]

write-host -ForegroundColor Cyan "Selected NIC0: " $ConnectionName $IntelName
write-host -ForegroundColor Cyan "Selected NIC1: " $ConnectionName1 $IntelName1

write-host -ForegroundColor Cyan (Get-DCBVersion -IntelName $IntelName )
Get-DCB-Map -ConnectionName $ConnectionName

Set-DCBEnviroment -IntelNICName $IntelName
Set-DCBWilling
Set-VLAN3260 -ConnectionName $ConnectionName
Set-IP -IP $sut -ConnectionName $ConnectionName
Clear-NetQoS
Set-iSCSI-Disk -DriveLetter 'R' -LinkPartner $lp -SUT $sut
Test-NetQosPolicy -LinkPartner $lp -SUT $sut



Start-Sleep 5
Write-host -ForegroundColor Cyan "Start test"

################################################################################


$packets=@('1514','4088','9014')
$i=1
$time=300
Foreach ($pack in $packets){
    Write-host -ForegroundColor Cyan "Step $i - Jumbo Packet: $pack"
    Set-NetAdapterAdvancedProperty -Name $ConnectionName -RegistryKeyword "*jumbopacket" -RegistryValue $pack
    if (((Get-NetAdapterAdvancedProperty -Name $ConnectionName  -DisplayName “Jumbo Packet”).RegistryValue) -eq ($pack)) {Write-host -ForegroundColor Green "Jumbo Packet $pack : Pass"} else {Write-host -ForegroundColor Red "Jumbo Packet $pack : Fail"}
Write-host -ForegroundColor Cyan "Step time $time sec" 
start-sleep 5
C:\DCB\Tools\IOMeter\IOmeter.exe C:\DCB\Tools\IOMeter\iometer.icf C:\DCB\Tools\IOMeter\results.cvs
for ($i=1;$i -lt $time;$i++){ start-sleep -s 1
Write-Progress -Activity "Progress test" -PercentComplete  $(($i/$time)*100) } 
Stop-Process -Name IOmeter -Force -Confirm:$false
$i++
}

Set-NetAdapterAdvancedProperty -Name $ConnectionName -RegistryKeyword "*jumbopacket" -RegistryValue '1514'
