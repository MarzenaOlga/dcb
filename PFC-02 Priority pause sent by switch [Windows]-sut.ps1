﻿
#$ComputerName = '10.102.229.166'
#& "C:\Users\Administrator\Documents\remote_session.ps1"

import-module 'C:\Program Files\Intel\Wired Networking\IntelNetCmdlets\IntelNetCmdlets.psd1'
import-module dcbqos
Import-Module DCB-test 

Write-Host -ForegroundColor cyan (Get-OS-Version)

$sut='192.168.1.50'
$lp ='192.168.1.10'

$ConnectionName=(Select-NIC)[0]
$IntelName=(Select-NIC)[1]
$ConnectionName1=(Select-NIC)[2]
$IntelName1=(Select-NIC)[3]

write-host -ForegroundColor Cyan "Selected NIC0: " $ConnectionName $IntelName
write-host -ForegroundColor Cyan "Selected NIC1: " $ConnectionName1 $IntelName1

write-host -ForegroundColor Cyan (Get-DCBVersion -IntelName $IntelName)
Get-DCB-Map -ConnectionName $ConnectionName

Set-DCBEnviroment -IntelNICName $IntelName
Set-DCBWilling
Set-VLAN3260 -ConnectionName $ConnectionName
Set-IP -IP $sut -ConnectionName $ConnectionName
Clear-NetQoS

Start-Sleep 5
Write-host -ForegroundColor Cyan "Start test"

Set-QoSPolicy -IP $sut

Start-Iperf -SutIPAddress $sut -LpIPAddress $lp -IntelNICName $IntelName
 
Import-Module C:\DCB\Tools\IntelNetDebugPS_Net45\IntelNetDebugPS_Net45.psd1

$x=0


do{
write-host $x
(Get-dpspfc).FormattedOutput
Start-sleep 10
$x = $x +1
}while ($x -ne 30)

Stop-Iperf 

