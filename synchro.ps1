﻿$SrcDir1="C:\DCB"
$SrcDir2="C:\posh\modules\DCB-Test"
$DestDir1= '\\10.237.94.251\Windows_Archive\Scripts\Posh - DCB\DCB'
$DestDir2= '\\10.237.94.251\Windows_Archive\Scripts\Posh - DCB\posh\modules\DCB-Test'


if (Test-Path \\10.237.94.251\Windows_Archive){
Copy-Item $SrcDir1\* -Destination $DestDir1 -Recurse -Force -Confirm:$false
Copy-Item $SrcDir2\* -Destination $DestDir2 -Recurse -Force -Confirm:$false
}
else { Write-host -ForegroundColor Red "Copy Failed"}