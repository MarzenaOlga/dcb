﻿
import-module 'C:\Program Files\Intel\Wired Networking\IntelNetCmdlets\IntelNetCmdlets.psd1'
import-module dcbqos
Import-Module DCB-test 

$ConnectionName=(Select-NIC)[0]
$IntelName=(Select-NIC)[1]
$ConnectionName1=(Select-NIC)[2]
$IntelName1=(Select-NIC)[3]

write-host -ForegroundColor Cyan "Selected NIC0: " $ConnectionName $IntelName
write-host -ForegroundColor Cyan "Selected NIC1: " $ConnectionName1 $IntelName1


Write-host -ForegroundColor Cyan "Start test"
$bandtwith=44
Start-sleep 10
$result =0
Get-DCB-Map -ConnectionName $ConnectionName
$y = (Get-NetAdapterQoS –Name $ConnectionName).OperationalTrafficClasses
if ($y -like "*1 ETS    $bandtwith%       4*")  {Write-host -ForegroundColor green "Test 3: OK"} else {Write-host -ForegroundColor red "Test Failed";$result = $result +1}

$z = (Get-IntelNetAdapterStatus -Status DCB -Name $IntelName) 
if (($z |Where-Object {$_.DisplayName -like 'iSCSI Bandwidth Percentage'}).DisplayValue -eq $bandtwith) {Write-host -ForegroundColor green "Test 4: OK"} else {Write-host -ForegroundColor red "Test Failed";$result = $result +10}


Set-DCBEnviroment -IntelNICName $IntelName
Set-DCBWilling
Clear-NetQoS
Get-IntelNetAdapterStatus -Status DCB -Name $IntelName

return $result
