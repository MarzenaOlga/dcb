﻿import-module 'C:\Program Files\Intel\Wired Networking\IntelNetCmdlets\IntelNetCmdlets.psd1'
import-module dcbqos
Import-Module DCB-test 

Write-Host -ForegroundColor cyan (Get-OS-Version)

$sut='192.168.1.50'
$lp ='192.168.1.10'

$ConnectionName=(Select-NIC)[0]
$IntelName=(Select-NIC)[1]
$ConnectionName1=(Select-NIC)[2]
$IntelName1=(Select-NIC)[3]

write-host -ForegroundColor Cyan "Selected NIC0: " $ConnectionName $IntelName
write-host -ForegroundColor Cyan "Selected NIC1: " $ConnectionName1 $IntelName1

write-host -ForegroundColor Cyan (Get-DCBVersion -IntelName $IntelName)
(Get-DCB-Map -ConnectionName $ConnectionName )

Set-DCBEnviroment -IntelNICName $IntelName
Set-DCBWilling
Set-VLAN3260 -ConnectionName $ConnectionName
Set-IP -IP $sut -ConnectionName $ConnectionName
Clear-NetQoS

Start-Sleep 5

Write-host -ForegroundColor Cyan "Start test"

Set-OSDCBEnviroment -IntelNICName $IntelName
Enable-NetQosFlowControl -Priority 0,1,2
New-NetQosTrafficClass -name "TC1" -priority 4   -bandwidthPercentage 30 -Algorithm ETS

if ((Get-NetQosDcbxSetting).Willing -like 'False'){Write-Host -ForegroundColor Green "Test 1 OK"} else {Write-Host -ForegroundColor Red "Test 1 Fail"}
(Get-IntelNetAdapterStatus -Name $IntelName -Status DCB)
if (((Get-NetAdapterQos).OperationalTrafficClasses -like '*0 ETS    70%       0-3,5-7*') -and ((Get-NetAdapterQos).OperationalTrafficClasses -like '*1 ETS    30%       4*')){Write-Host -ForegroundColor Green "Test 2 OK"} else {Write-Host -ForegroundColor Red "Test 2 Fail"}

Set-DCBEnviroment -IntelNICName $IntelName
Set-DCBWilling